import sys
import subprocess
import os

# Nous avons préférer diffuser un script d'installation plutôt que le script d'exécution afin de pouvoir modifier notre code source sans avoir à
# retransmettre à l'ensemble des utilisateurs les packets mis à jour
# Variables
git_repo = "https://gitlab.com/Chopanaweur/majeure_messagerie_chiffr-e.git"
local_repo = os.getcwd()+"/g11-majeure"
path_web_exe = local_repo+"/GUI"  

def upgrade_pip():
    try:
        print("Tentative de mise a jour de pip")
        subprocess.check_call([sys.executable, "-m", "pip", "install", "--upgrade", " pip"])
        print("Pip a bien ete mis a jour")
    except:
        print("Erreur lors de la mise a jour de pip")

# Requirement : pip + git somewhere
# Installe le paquet s il n est pas present
def install_package(package):
    try:
        print("Installation de", package,"en cours")
        subprocess.check_call([sys.executable, "-m", "pip", "install", package])
        print("Le paquet ",package,"a ete correctement installe !")
    except:
        print("Erreur d installation du package :",package)

# Verifie si le paquet est present, sinon nous l installons
def check_package(package):
    try:
        subprocess.check_call([sys.executable, "-m", "pip","show", package])
    except:
        install_package(package)

def execute(path):
    os.chdir(path)
    os.system("python main.py")
    
if __name__ == "__main__" :
    print(local_repo)
    print("##### Installation des packages #####")
    upgrade_pip()
    check_package("gitpython") # verifie la presence du paquet. Le cas echeant on l installe
    check_package("eel")
    check_package("pywebview")
    check_package("pypiwin32")
    check_package("pefile")

    import git # a importer apres checkpackage gitpython pour ne pas importer un module non present

    # Si le depot git n existe pas en local, on le clone sinon on execute la commande "git pull"
    print("##### Recuperation du depot git #####")
    if not (os.path.exists(local_repo)):
        git.Repo.clone_from(git_repo, local_repo, branch='master')
    else :
        repo = git.Repo(local_repo)
        repo.remotes.origin.pull()
        print("Le projet etait present sur votre machine, il a ete mis a jour.")

    execute(path_web_exe)
