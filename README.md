# Intallation
Installation guide for Linux

### Installing python3
`sudo apt install python3`

### Cloning the git repo
`git clone https://gitlab.com/Chopanaweur/majeure_messagerie_chiffr-e`

### Proceeding to Installation
`cd majeure_messagerie_chiffr-e/`

`sudo python3 installer/install_majeure.py`

The application should launch proprely after installing the requiered phython librairies

# Uninstallation

### Installing pip-autoremove
pip-autoremove is used to remove librairies and their dependencies.

`sudo pip3 install pip-autoremove`

Now remove requiered packages :

`pip-autoremove pywebview gitpython eel`

Then remove installation git folder

`sudo rm -r majeure_messagerie_chiffr-e/`
