# Majeure messagerie chiffrées GUI

Graphic user interface for messagerie chiffrées project

Based on python3 with Eel module.

Simply click on `launch.bat` to launch the application in standalone mode (without need to install python).