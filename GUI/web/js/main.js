// ****************** Return functions when calling Py ******************

// 0 == not vulnerable ; 1 == vulnerable
function is_my_version_vulnerable(client_name, ret){
	var num_vers = ret.split(".");
	
	// v52.9
	if (client_name === "Thunderbird"){ 
		if ((num_vers[0]>52) || (num_vers[0] == 52 && num_vers[1] >= 9)) {
			return 0
		}
		
		else {
			return 1
		}
	}
	
	// v7.1.3
	else if (client_name === "eM_Client"){
		if ((num_vers[0]>7) || (num_vers[0] == 7 && num_vers[1] > 1) || (num_vers[0] == 7 && num_vers[1] == 1 && num_vers[2] >= 3)) {
			return 0
		}
		
		else {
			return 1
		}
	}
	
	// v6.1.5
	else if (client_name === "Postbox"){
		if ((num_vers[0]>6) || (num_vers[0] == 6 && num_vers[1] > 1) || (num_vers[0] == 6 && num_vers[1] == 1 && num_version[2] >=5)) {
			return 0
		}
		
		else {
			return 1
		}
	}
	
	// ne supporte pas le html / css
	else if (client_name == "Claws"){
		return 0
	}
	
	// Mail en cli, supporte uniquement le texte brut
	else if (client_name == "Mutt"){
		return 0 
	}
	
	else {
		return 1
	}
}

// affiche le contenu de la page en fonction de la vulnerabilite du client selectionne
function return_get_client_version(client_name, ret){
	// cas ou lexecutable nest pas trouver
	if(ret == -1){
		
		$("#version").hide();
		$("#path").show();
		$("#vulnerable_no").show();
		$("#vulnerable_yes").show();
		$("#recommandations").show();
		$("#recommandations_claws_mutt").hide();
		
		if ((client_name == "Claws") || (client_name == "Mutt")) {
			$("#vulnerable_no").show();
			$("#vulnerable_yes").hide();
			$("#recommandations").hide();
			$("#recommandations_claws_mutt").show();
		}
		
		// Default path not found
		$('#modal_executable_not_found_client').html(client_name) 
		$('#modal_executable_not_found').modal('show')
	}
	
	// si on trouve lexecutable 
	else{
		$("#version").show();
		$("#path").hide();
		$("#recommandations_claws_mutt").hide();
		
		// Hide yes or no on client page ( 1 == vulnerable ; else == not vulnerable )
		if (is_my_version_vulnerable(client_name, ret) == 1) {
			$("#vulnerable_no").hide();
			$("#vulnerable_yes").show();
			$("#recommandations").show();
		}
		else {
			$("#vulnerable_yes").hide();
			$("#vulnerable_no").show();
			$("#recommandations").hide();
		}
		
		$("#version_number").html(ret)
	}
}

function return_get_file_path(ret){
	// Retry get version number with newly selected path
	pywebview.api.get_client_version(["custom", ret]).then(return_get_client_version.bind(null, "custom"));
}

// ****************** Normal code execution ******************

function hello_world(){
	pywebview.api.hello_world("Hello py !").then(return_helloworld);
}

function change_to_client_details_view(client_name){
	// Get zones
	clients_zone = document.getElementById('clients')
	client_details_zone = document.getElementById('client_details')
	back_arrow = document.getElementById('back_arrow')

	// Update client name in page to match button clicked by user
	client_name_zones = document.getElementsByClassName("client_name")
	for(var i = 0; i < client_name_zones.length; i++)
	{
	   client_name_zones.item(i).innerHTML = client_name;
	}

	// Get version number
	pywebview.api.get_client_version([client_name, "null"]).then(return_get_client_version.bind(null, client_name));

	// Hide client selection and display client details
	clients_zone.style.display = "none" ;
	client_details_zone.style.display = "block" ;
	back_arrow.style.visibility = "visible" ;

}

function return_to_client_selection(){
	// Get zones
	clients_zone = document.getElementById('clients')
	client_details_zone = document.getElementById('client_details')
	back_arrow = document.getElementById('back_arrow')

	// Hide client selection and display client details
	clients_zone.style.display = "block" ;
	client_details_zone.style.display = "none" ;
	back_arrow.style.visibility = "hidden" ;
}

function get_file_path(){
	pywebview.api.get_file_path("toto").then(return_get_file_path)
}