import json
import webview
import threading
import time
from win32com.client import Dispatch
from os import path
import pefile
import re

def get_version_via_com(filename):
    version = -1
    if path.exists(filename):
        parser = Dispatch("Scripting.FileSystemObject")
        version = parser.GetFileVersion(filename)
    return version

def get_version_via_pefile_Info(filename):
    version = -1
    if path.exists(filename):
        pe = pefile.PE(filename)
        info = pe.dump_info()
        reg = re.compile(r"ProductVersion: (\d+\.\d+\.\d+)")
        version = reg.findall(info)
    return version[0]

class Api():

    def get_client_version(self, data):
        name = data[0]
        alt_path = data[1]
        ret = -1
        print("Client name received : " + name)
        if name == "Outlook":
            path = r"C:\Program Files\Microsoft Office\Office16\OUTLOOK.EXE"
            ret = get_version_via_com(path)
        elif name == "The_Bat!":
            path = r"C:\Program Files\The Bat!\thebat64.exe"
            ret = get_version_via_com(path)
        elif name == "eM_Client":
            path = r"C:\Program Files (x86)\eM Client\MailClient.exe"
            ret = get_version_via_com(path)
        elif name == "custom":
            path = alt_path
            ret = get_version_via_com(path)
        elif name == "Thunderbird":
            path = r"C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe"
            ret = get_version_via_com(path)
        elif name == "Postbox":
            path = r"C:\Program Files (x86)\Postbox\postbox.exe"
            ret = get_version_via_pefile_Info(path)

        # numeros de patch introuvable en ligne
        # nous appelons tout de meme la fonction pour actualiser
        # laffichage de la page et ne pas afficher le numero de version
        # du client affiche precedemment 
        elif name == "kMail":
            path = "fjdgsfgiorjdij"
            ret = get_version_via_com(path)

        elif name == "Windows10_Mail":
            path = "fjdgsfgiorjdij"
            ret = get_version_via_com(path)

        elif name == "Windows_Live_Mail":
            path = "fjdgsfgiorjdij"
            ret = get_version_via_com(path)
            
        elif name == "Evolution":
            path = "fjdgsfgiorjdij"
            ret = get_version_via_com(path)

        elif name == "Trojita":
            path = "fjdgsfgiorjdij"
            ret = get_version_via_com(path)
        
        # peut importe le chemin des deux clients suivants car ils n ont jamais
        # ete vulnerables
        elif name == "Mutt":
            path = r"fijogjsidjg34"
            ret = get_version_via_com(path)
        elif name == "Claws":
            path = r"fijogjsidjg34"
            ret = get_version_via_com(path)
        
        else :
            ret = 0
        print("Version=" + str(ret))
        return ret

    def get_file_path(self, toto):
        path = window.create_file_dialog(webview.OPEN_DIALOG, allow_multiple=False)
        return path[0]

if __name__ == '__main__':

    # **** Initialize webview ****
    api = Api()
    window = webview.create_window('Majeure Messageries Chiffrées', 'web/index.html', js_api=api)
    webview.start(debug=True)
